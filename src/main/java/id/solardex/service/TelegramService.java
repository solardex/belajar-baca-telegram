package id.solardex.service;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import id.solardex.dto.TelegramResponseEntity;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service @Slf4j
public class TelegramService {
	String token = "your_token";
	public void fetchUpates() {
		log.info("---------- START ");
		Mono<TelegramResponseEntity> hasil = WebClient.create()
				.get()
				.uri("https://api.telegram.org/bot" + token + "/getUpdates")
				.retrieve()
				.bodyToMono(TelegramResponseEntity.class);
    	hasil.subscribe(telegramResponseEntity -> processTelegram(telegramResponseEntity));
    	log.info("---------- FINISH ");
	}
	private void processTelegram(TelegramResponseEntity telegramResponseEntity) {
		if (telegramResponseEntity.isOk()) {
			log.info("result size: " + telegramResponseEntity.getResult().size());
			telegramResponseEntity.getResult().forEach(it -> {
				Date tgl = new Date(it.getMessage().getDate() * 1000);
				log.info(it.getUpdate_id() + " - " + tgl);
			});
		}
		else {
			log.info("Error " + telegramResponseEntity.getError_code() + ": " + telegramResponseEntity.getDescription());
		}
	}
}
