package id.solardex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import id.solardex.service.TelegramService;
import lombok.extern.slf4j.Slf4j;

@Controller 
public class MainController {
	@Autowired TelegramService telegramService;
    @GetMapping("/getUpdates")
    public String getUpdates() {
    	telegramService.fetchUpates();
        return "get-updates";
    }	
}
