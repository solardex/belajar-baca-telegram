package id.solardex.dto;

import java.util.List;

import lombok.Data;

@Data
public class TelegramResponseEntity {
	boolean ok;
	List<TelegramResult> result;
	Integer error_code;
	String description;
}
