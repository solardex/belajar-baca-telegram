package id.solardex.dto;

import lombok.Data;

@Data
public class TelegramResult {
	long update_id;
	TelegramMessage message;
}
