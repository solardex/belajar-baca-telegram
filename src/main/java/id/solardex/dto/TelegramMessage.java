package id.solardex.dto;

import java.util.Date;

import lombok.Data;

@Data
public class TelegramMessage {
	long message_id;
	long date;
	TelegramChat from;
	TelegramChat chat;
	String text;
}
