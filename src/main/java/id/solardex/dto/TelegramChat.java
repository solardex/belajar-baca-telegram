package id.solardex.dto;

import lombok.Data;

@Data
public class TelegramChat {
	long id;
	boolean is_bot;
	String first_name;
	String username;
	String language_code;
	String type;
}
